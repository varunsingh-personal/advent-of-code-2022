import re

stacks = [
    ['S', 'M', 'R', 'N', 'W', 'J', 'V', 'T'],
    ['B', 'W', 'D', 'J', 'Q', 'P', 'C', 'V'],
    ['B', 'J', 'F', 'H', 'D', 'R', 'P'],
    ['F', 'R', 'P', 'B', 'M', 'N', 'D'],
    ['H', 'V', 'R', 'P', 'T', 'B'],
    ['C', 'B', 'P', 'T'],
    ['B', 'J', 'R', 'P', 'L'],
    ['N', 'C', 'S', 'L', 'T', 'Z', 'B', 'W'],
    ['L', 'S', 'G']
]

with open('input.txt', 'r') as inp_file:
    inp_txt = inp_file.read()

pattern = r"^move (?P<count>\d+) from (?P<src>\d+) to (?P<dst>\d+)$"


def part_one():
    for line in inp_txt.split('\n'):

        match = re.match(pattern, line)
        count = int(match.group("count"))
        src = int(match.group("src"))
        dst = int(match.group("dst"))
        for i in range(0, count):
            stacks[dst - 1].append(stacks[src - 1].pop())

    for i in range(0, len(stacks)):
        print(stacks[i].pop(), end='')


def part_two():
    for line in inp_txt.split('\n'):

        match = re.match(pattern, line)
        count = int(match.group("count"))
        src = int(match.group("src"))
        dst = int(match.group("dst"))
        temp_stack = []
        for i in range(0, count):
            temp_stack.append(stacks[src - 1].pop())
        for i in range(0, count):
            stacks[dst - 1].append(temp_stack.pop())

    for i in range(0, len(stacks)):
        print(stacks[i].pop(), end='')


part_two()
