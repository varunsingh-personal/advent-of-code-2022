with open("input.txt", "r") as inp_file:
    inp_txt = inp_file.read()

start_marker = 0
for i in range(0, len(inp_txt) - 14):
    bucket = set()
    for j in range(i, i + 14):
        c = inp_txt[j]
        if c not in bucket:
            bucket.add(c)
        else:
            break
    else:
        start_marker = i
        print(start_marker + 14)
        exit(0)

