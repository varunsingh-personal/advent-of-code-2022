def get_input(filename) -> list:
    with open(filename, 'r') as inp_file:
        inp_lines = inp_file.read().split('\n')

    instruction = []
    for line in inp_lines:
        # print(line)
        line_parts = line.split(' ')
        op = line_parts[0]
        if len(line_parts) > 1:
            data = int(line_parts[1])
        else:
            data = None
        instruction.append((op, data))

    return instruction


def part_one():
    data_input = get_input('input.txt')
    cycle = 0
    input_sum = 1
    signal_strengths = []
    target_cycles = [20, 60, 100, 140, 180, 220]

    for (op, data) in data_input:
        if op == 'noop':
            cycle += 1
            if cycle in target_cycles:
                signal_strengths.append(input_sum * cycle)
        else:
            cycle += 1
            if cycle in target_cycles:
                signal_strengths.append(input_sum * cycle)
            cycle += 1
            if cycle in target_cycles:
                signal_strengths.append(input_sum * cycle)
            input_sum += data

    print(sum(signal_strengths))


def part_two():
    lit = '#'
    dark = ' '
    display = [[]]
    target_cycles = [40, 80, 120, 160, 200, 240]
    data_input = get_input('input.txt')
    cycle = 0
    sprite_pos = 1
    sprite_list = [0, 1, 2]
    draw_at = 0

    display_row = 0
    for (op, data) in data_input:
        if op == 'noop':
            if draw_at in sprite_list:
                display[display_row].insert(draw_at, lit)
            else:
                display[display_row].insert(draw_at, dark)
            cycle += 1
            draw_at += 1
            if cycle in target_cycles:
                display_row += 1
                draw_at = 0
                display.append([])
        else:
            if draw_at in sprite_list:
                display[display_row].insert(draw_at, lit)
            else:
                display[display_row].insert(draw_at, dark)

            draw_at += 1
            cycle += 1

            if cycle in target_cycles:
                display_row += 1
                draw_at = 0
                display.append([])

            if draw_at in sprite_list:
                display[display_row].insert(draw_at, lit)
            else:
                display[display_row].insert(draw_at, dark)

            draw_at += 1
            cycle += 1

            if cycle in target_cycles:
                display_row += 1
                draw_at = 0
                display.append([])

            sprite_pos += data
            sprite_list = [sprite_pos - 1, sprite_pos, sprite_pos + 1]

    for i in range(0, 6):
        for j in range(0, 40):
            print(display[i][j], end=dark)
        print()


if __name__ == '__main__':
    part_two()
