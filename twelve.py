from __future__ import annotations
import copy
import os
import string
import time

S = 1
E = 27


class Node:
    def __init__(self, i, j, value, char, visited=False):
        self.i = i
        self.j = j
        self.value = value
        self.char = char
        self.visited = visited
        self.path: list[Node] = []

    @staticmethod
    def copy(src: Node, dst: Node) -> Node:
        dst.i = src.i
        dst.j = src.j
        dst.value = src.value
        dst.char = src.char
        dst.path = []
        for node in src.path:
            dst.path.append(node)
        return dst

    def __repr__(self):
        return self.char + ' ' + str(self.i) + ' ' + str(self.j)

    def __eq__(self, other):
        return self.i == other.i and self.j == other.j

    def __cmp__(self, other):
        return len(self.path) - len(other.path)

    def __hash__(self):
        return hash(self.char + ' ' + str(self.i) + ' ' + str(self.j))


def get_input() -> (dict[str: Node], list[list[Node]]):
    atoz = dict(zip(string.ascii_lowercase, range(1, 27)))
    nodes = {}
    arr = []
    with open('input.txt', 'r') as inp_file:
        inp_lines = inp_file.read().split('\n')
        for i in range(0, len(inp_lines)):
            new_row = []
            for j in range(0, len(inp_lines[i])):
                char = inp_lines[i][j]
                if char == 'S':
                    val = S
                elif char == 'E':
                    val = E
                else:
                    val = atoz[char]
                n = Node(i=i, j=j, value=val, char=char)
                new_row.append(n)
                nodes[str(i) + '#' + str(j)] = n
            arr.append(new_row)

    return nodes, arr


def get_node(i: int, j: int, nodes: dict[str: Node]) -> Node:
    return nodes[str(i) + '#' + str(j)]


def traverse(data: list[list[Node]], stack: list[Node], paths):
    visited_nodes = []
    while stack:
        node = stack.pop()
        if node.char == 'E':
            paths.append(node.path)
            visited_nodes = []
            continue
        if not visited_nodes:
            visited_nodes.extend(list(node.path))
        else:
            visited_nodes.append(node)
        node, next_nodes = get_next_nodes(node, visited_nodes, data)

        push_nodes(node, next_nodes, stack)


def print_formatted(i, j, data: list[list[Node]]):
    os.system('clear')
    for r in range(0, len(data)):
        for c in range(0, len(data[i])):
            if r == i and c == j:
                print('\u001b[31m{}\u001b[0m'.format(data[r][c].char), end='')
            else:
                print(data[r][c].char, end='')
        print()
    print()
    os.system('sleep 0.125')


def get_neighbors(n: Node, data: list[list[Node]]) -> list[Node]:
    ret_val = []
    i = n.i
    j = n.j
    if i - 1 >= 0:
        ret_val.append(data[i - 1][j])
    if i + 1 < len(data):
        ret_val.append(data[i + 1][j])
    if j - 1 >= 0:
        ret_val.append(data[i][j - 1])
    if j + 1 < len(data[i]):
        ret_val.append(data[i][j + 1])
    return ret_val


def push_nodes(node: Node, nodes: list[Node], stack: list[Node]):
    for n in nodes:
        n.path = list(node.path)
        n.path.append(n)
        stack.append(copy.deepcopy(n))


def get_next_nodes(node: Node, visited_nodes: list[Node], data: list[list[Node]]) -> (Node, list[Node]):
    nodes = []
    while not nodes:
        neighbors = get_neighbors(node, data)
        not_visited = list(filter(lambda x: x not in visited_nodes, neighbors))
        nodes = list(filter(lambda x: x.value - node.value <= 1, not_visited))
        nodes = sorted(nodes, key=lambda x: x.value, reverse=True)
        if not nodes:
            if node.path:
                node = node.path.pop()
            else:
                break

    return node, nodes


def valid_path(path: list[Node], data: list[list[Node]]) -> bool:
    for i in range(0, len(path) - 1):
        curr_node = path[i]
        next_node = path[i + 1]
        if curr_node not in get_neighbors(next_node, data):
            return False
    return True


def find_start_node(data: list[list[Node]]) -> (int, int):
    for row in data:
        for col in row:
            if col.char == 'S':
                return col.i, col.j


def part_one():
    nodes, array = get_input()
    start_i, start_j = find_start_node(array)
    array[start_i][start_j].path = [array[start_i][start_j]]
    stack = [array[start_i][start_j]]
    paths = []
    start_time = time.time()
    traverse(array, stack, paths)
    end_time = time.time()
    print("time taken: {}".format(end_time - start_time))
    print(len(min(paths, key=lambda x: len(x))))


if __name__ == '__main__':
    part_one()
