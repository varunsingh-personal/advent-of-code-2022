import sys
from enum import Enum

DIR = 'DIR'
FILE = 'FILE'
CD_DOWN = 'CD_DOWN'
CD_UP = 'CD_UP'
LS = 'LS'


class File:
    def __init__(self, name, size, is_dir, parent_dir, children=None):
        self.name = name
        self.size = size
        self.is_dir = is_dir
        self.parent_dir = parent_dir
        if not children:
            self.children = {}

    def __repr__(self):
        return '-- ' + self.name + ' ' + str(self.size) + ' ' + str(self.is_dir) + ' ' + str(self.parent_dir.name if self.parent_dir else '') + ' --'
    # def __repr__(self):
    #     return str(self.size)


class Operation(Enum):
    NOOP = 0
    DIR_CHANGE = 1
    BROWSE = 2


def find_type(inp_line: str):
    if inp_line.startswith('$ cd ..'):
        item_type = CD_UP
    elif inp_line.startswith('$ cd '):
        item_type = CD_DOWN
    elif inp_line.startswith('$ ls'):
        item_type = LS
    elif inp_line.startswith('d'):
        item_type = DIR
    else:
        item_type = FILE
    return item_type


def name_from_cd(line: str):
    return line.split(' ')[2]


def file_name(line: str):
    return line.split(' ')[1]


def file_size(line: str):
    return int(line.split(' ')[0])


with open('input.txt', 'r') as inp_file:
    inp_lines = inp_file.read().split('\n')

flat_files = []


def calc_size(node: File):
    flat_files.append(node)
    if not len(node.children):
        return node.size
    else:
        total_size = node.size
        for value in node.children.values():
            node_size = calc_size(value)
            total_size += node_size
            node.size = total_size
        return total_size


def build_filesystem():
    root = None
    curr_dir = None
    for l in inp_lines:
        itype = find_type(l)
        if itype is CD_DOWN:
            curr_item = File(name=name_from_cd(l), size=0, is_dir=True, parent_dir=curr_dir) if not curr_dir else \
            curr_dir.children[name_from_cd(l)]
            curr_dir = curr_item
            if not root:
                root = curr_item
        elif itype is DIR:
            new_item = File(name=file_name(l), size=0, is_dir=True, parent_dir=curr_dir)
            curr_dir.children[file_name(l)] = new_item
        elif itype is FILE:
            new_item = File(name=file_name(l), size=file_size(l), is_dir=False, parent_dir=curr_dir)
            curr_dir.children[file_name(l)] = new_item
        elif itype is CD_UP:
            curr_dir = curr_dir.parent_dir
    return root


def part_one():
    global flat_files
    flat_files = []
    fs_root = build_filesystem()
    calc_size(fs_root)
    print(sum([f.size for f in flat_files if f.is_dir and f.size <= 100_000]))


def part_two():
    global flat_files
    flat_files = []
    fs_root = build_filesystem()
    calc_size(fs_root)
    total_consumed_size = fs_root.size
    max_size = 70_000_000
    desired_size = 30_000_000
    free_size = max_size - total_consumed_size
    size_to_free = desired_size - free_size
    print("total_consumed_size: {}".format(total_consumed_size))
    print("max_size: {}".format(max_size))
    print("desired_size: {}".format(desired_size))
    print("free_size: {}".format(free_size))
    print("size_to_free: {}".format(size_to_free))

    smallest_file = None
    min_size_diff = sys.maxsize
    print([f for f in flat_files if f.is_dir and f.size > size_to_free])
    smallest_file = min([f for f in flat_files if f.is_dir and f.size > size_to_free], key=lambda x: x.size)
    # for f in candidates:
    #     print(f)
    #     size_diff = f.size - size_to_free
    #     if size_diff < min_size_diff:
    #         smallest_file = f

    print("smallest_file: {}".format(smallest_file))


part_two()
