from __future__ import annotations


def get_input(filename) -> list:
    with open(filename, 'r') as inp_file:
        inp_lines = inp_file.read().split('\n')

    moves = []
    for line in inp_lines:
        moves.append((line.split(' ')[0], int(line.split(' ')[1])))

    return moves


class Knot:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.history = set()

    def __repr__(self):
        return str(self.x) + ' ' + str(self.y)

    def add_history(self):
        self.history.add("{}#{}".format(self.x, self.y))

    def move(self, direction: str):
        if direction == 'R':
            self.x += 1
        elif direction == 'L':
            self.x -= 1
        elif direction == 'U':
            self.y += 1
        else:
            self.y -= 1
        self.add_history()

    def follow(self, knot: Knot):
        if knot.x - self.x >= 2 and knot.y - self.y == 0:
            self.x += 1
            self.y += 0
        elif knot.x - self.x <= -2 and knot.y - self.y == 0:
            self.x -= 1
            self.y += 0
        elif knot.x - self.x == 0 and knot.y - self.y >= 2:
            self.x += 0
            self.y += 1
        elif knot.x - self.x == 0 and knot.y - self.y <= -2:
            self.x += 0
            self.y -= 1
        elif knot.x - self.x <= -2 and knot.y - self.y >= 1:
            self.x -= 1
            self.y += 1
        elif knot.x - self.x >= 2 and knot.y - self.y >= 1:
            self.x += 1
            self.y += 1
        elif knot.x - self.x >= 2 and knot.y - self.y <= -1:
            self.x += 1
            self.y -= 1
        elif knot.x - self.x <= -2 and knot.y - self.y <= -1:
            self.x -= 1
            self.y -= 1
        elif knot.x - self.x >= 1 and knot.y - self.y >= 2:
            self.x += 1
            self.y += 1
        elif knot.x - self.x <= -1 and knot.y - self.y <= -2:
            self.x -= 1
            self.y -= 1
        elif knot.x - self.x <= -1 and knot.y - self.y >= 2:
            self.x -= 1
            self.y += 1
        elif knot.x - self.x >= 1 and knot.y - self.y <= -2:
            self.x += 1
            self.y -= 1
        self.add_history()


def part_one():
    head = Knot()
    tail = Knot()
    print(get_input('input.txt'))
    for direction, units in get_input('input.txt'):
        for u in range(0, units):
            head.move(direction)
            tail.follow(head)

    print(len(tail.history))


def part_two():
    rope = []
    for i in range(0, 10):
        rope.append(Knot())
    head = rope[0]
    tail = rope[len(rope) - 1]
    for direction, units in get_input('input.txt'):
        for u in range(0, units):
            head.move(direction)
            for i in range(1, len(rope)):
                rope[i].follow(rope[i - 1])
    print(len(tail.history))


if __name__ == '__main__':
    part_two()
