import string

lowercase_items = dict(zip(string.ascii_lowercase, range(1, 27)))
uppercase_items = dict(zip(string.ascii_uppercase, range(27, 53)))

with open('input.txt', 'r') as input_file:
    input_text = input_file.read()

input_lines = input_text.split('\n')


def part_one():
    sum_items = 0
    for line in input_lines:
        first_half = line[:int(len(line) / 2)]
        second_half = line[int(len(line) / 2):]
        item = (set(first_half).intersection(set(second_half))).pop()
        sum_items += lowercase_items[item] if item in lowercase_items else uppercase_items[item]

    print(sum_items)


def part_two():
    sum_items = 0
    for i in range(0, len(input_lines) - 2, 3):
        first_line = set(input_lines[i])
        second_line = set(input_lines[i + 1])
        third_line = set(input_lines[i + 2])
        item = first_line.intersection(second_line.intersection(third_line)).pop()
        sum_items += lowercase_items[item] if item in lowercase_items else uppercase_items[item]
    print(sum_items)


part_two()
