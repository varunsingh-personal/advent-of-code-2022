from __future__ import annotations

import time
from queue import Queue


class Monkey:
    def __init__(self, items: Queue, op: dict, test: dict):
        self.items = items
        self.op = op
        self.test = test
        self.inspect_count = 0
        self.head = -1
        self.tail = -1

    def __repr__(self):
        return str(self.inspect_count)

    def put_items(self, *args):
        for arg in args:
            self.items.put_nowait(int(arg))

    def get_item(self) -> int:
        item = self.items.get_nowait()
        return item

    def give_item_to(self, item, other_monkey: Monkey):
        other_monkey.put_items(item)

    def inspect_item(self, item: int, lcm: int) -> int:
        self.inspect_count += 1
        ret_val = 0
        op = self.op
        sign = op["sign"]
        arg = op['arg']
        reflexive = op['self']
        match sign:
            case '*':
                if not reflexive:
                    ret_val = item * arg
                else:
                    ret_val = item * item
            case '/':
                if not reflexive:
                    ret_val = item / arg
                else:
                    ret_val = item / item
            case '+':
                if not reflexive:
                    ret_val = item + arg
                else:
                    ret_val = item + item
            case '-':
                if not reflexive:
                    ret_val = item - arg
                else:
                    ret_val = item - item
            case '%':
                if not reflexive:
                    ret_val = item % arg
                else:
                    ret_val = item % item
        return int(ret_val % lcm)

    def test_item(self, item: int) -> int:
        ret_val = 0
        result = 0
        test = self.test
        sign = test["sign"]
        arg = test['arg']
        reflexive = test['self']
        true = test['true']
        false = test['false']
        match sign:
            case '*':
                if not reflexive:
                    result = item * arg
                else:
                    result = item * item
            case '/':
                if not reflexive:
                    result = item / arg
                else:
                    result = item / item
            case '+':
                if not reflexive:
                    result = item + arg
                else:
                    result = item + item
            case '-':
                if not reflexive:
                    result = item - arg
                else:
                    result = item - item
            case '%':
                if not reflexive:
                    result = item % arg
                else:
                    result = item % item

        return true if ret_val == result else false


def get_input() -> (int, list):
    monkeys = [Monkey(Queue(maxsize=-1), {"sign": '*', "arg": 11, "self": False},
                      {"sign": '%', "arg": 7, 'self': False, 'result': 0, 'true': 6, 'false': 2}),
               Monkey(Queue(maxsize=-1), {"sign": '+', "arg": 1, "self": False},
                      {"sign": '%', "arg": 11, 'self': False, 'result': 0, 'true': 5, 'false': 0}),
               Monkey(Queue(maxsize=-1), {"sign": '*', "arg": 7, "self": False},
                      {"sign": '%', "arg": 13, 'self': False, 'result': 0, 'true': 4, 'false': 3}),
               Monkey(Queue(maxsize=-1), {"sign": '+', "arg": 3, "self": False},
                      {"sign": '%', "arg": 3, 'self': False, 'result': 0, 'true': 1, 'false': 7}),
               Monkey(Queue(maxsize=-1), {"sign": '+', "arg": 6, "self": False},
                      {"sign": '%', "arg": 17, 'self': False, 'result': 0, 'true': 3, 'false': 7}),
               Monkey(Queue(maxsize=-1), {"sign": '+', "arg": 5, "self": False},
                      {"sign": '%', "arg": 2, 'self': False, 'result': 0, 'true': 0, 'false': 6}),
               Monkey(Queue(maxsize=-1), {"sign": '*', "arg": None, "self": True},
                      {"sign": '%', "arg": 5, 'self': False, 'result': 0, 'true': 2, 'false': 4}),
               Monkey(Queue(maxsize=-1), {"sign": '+', "arg": 7, "self": False},
                      {"sign": '%', "arg": 19, 'self': False, 'result': 0, 'true': 5, 'false': 1})
               ]

    monkeys[0].put_items(63, 57)
    monkeys[1].put_items(82, 66, 87, 78, 77, 92, 83)
    monkeys[2].put_items(97, 53, 53, 85, 58, 54)
    monkeys[3].put_items(50)
    monkeys[4].put_items(64, 69, 52, 65, 73)
    monkeys[5].put_items(57, 91, 65)
    monkeys[6].put_items(67, 91, 84, 78, 60, 69, 99, 83)
    monkeys[7].put_items(58, 78, 69, 65)
    lcm = 1
    for mon in monkeys:
        lcm *= mon.test['arg'] if mon.test['arg'] else 1
    return lcm, monkeys


def part_one():
    lcm, monkeys = get_input()
    for i in range(0, 10_000):
        for mon in monkeys:
            while not mon.items.empty():
                item = mon.get_item()
                item = mon.inspect_item(item, lcm)
                give_to = mon.test_item(item)
                mon.give_item_to(item, monkeys[give_to])

    sorted_monkeys = sorted(monkeys, key=lambda x: x.inspect_count, reverse=True)
    print(sorted_monkeys[0].inspect_count * sorted_monkeys[1].inspect_count)


if __name__ == '__main__':
    part_one()
