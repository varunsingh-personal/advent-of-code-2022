import unittest
import eight


class DayEightTests(unittest.TestCase):

    ############################################################
    def test_when_tallest_col_tree_then_tree_visible(self):
        forest = [[0, 2, 3, 6, 5, 4]]
        self.assertEqual(True, eight.tree_visible_col(forest, 0, 3))

    def test_when_shortest_col_tree_then_tree_not_visible(self):
        forest = [[0, 2, 3, 1, 5, 4]]
        self.assertEqual(False, eight.tree_visible_col(forest, 0, 3))

    def test_when_same_height_col_tree_then_tree_not_visible(self):
        forest = [[0, 2, 5, 5, 5, 4]]
        self.assertEqual(False, eight.tree_visible_col(forest, 0, 3))

    def test_when_shortest_col_tree_after_edge_then_tree_not_visible(self):
        forest = [[6, 2, 5, 5, 5, 4]]
        self.assertEqual(False, eight.tree_visible_col(forest, 0, 1))

    def test_when_tallest_col_tree_after_edge_then_tree_visible(self):
        forest = [[6, 7, 5, 5, 5, 4]]
        self.assertEqual(True, eight.tree_visible_col(forest, 0, 1))

    def test_when_shortest_col_tree_before_edge_then_tree_not_visible(self):
        forest = [[6, 2, 5, 5, 3, 4]]
        self.assertEqual(False, eight.tree_visible_col(forest, 0, 4))

    def test_when_tallest_col_tree_before_edge_then_tree_visible(self):
        forest = [[6, 6, 5, 5, 7, 4]]
        self.assertEqual(True, eight.tree_visible_col(forest, 0, 4))

    ############################################################

    def test_when_tallest_row_tree_then_tree_visible(self):
        forest = [[0], [2], [3], [6], [5], [4]]
        self.assertEqual(True, eight.tree_visible_row(forest, 3, 0))

    def test_when_shortest_row_tree_then_tree_not_visible(self):
        forest = [[0], [2], [3], [1], [5], [4]]
        self.assertEqual(False, eight.tree_visible_row(forest, 3, 0))

    def test_when_same_height_row_tree_then_tree_not_visible(self):
        forest = [[0], [2], [5], [5], [5], [4]]
        self.assertEqual(False, eight.tree_visible_row(forest, 3, 0))

    def test_when_short_row_tree_after_edge_then_tree_not_visible(self):
        forest = [[6], [2], [5], [5], [5], [4]]
        self.assertEqual(False, eight.tree_visible_row(forest, 1, 0))

    def test_when_tall_row_tree_after_edge_then_tree_visible(self):
        forest = [[6], [7], [5], [5], [5], [4]]
        self.assertEqual(True, eight.tree_visible_row(forest, 1, 0))

    def test_when_short_row_tree_before_edge_then_tree_not_visible(self):
        forest = [[6], [2], [5], [5], [3], [4]]
        self.assertEqual(False, eight.tree_visible_row(forest, 4, 0))

    def test_when_tall_row_tree_before_edge_then_tree_visible(self):
        forest = [[6], [6], [5], [5], [7], [4]]
        self.assertEqual(True, eight.tree_visible_row(forest, 4, 0))

    def test_tallest_tree_visible_in_forest(self):
        forest = [[3, 0, 3, 7, 3],
                  [2, 5, 5, 1, 2],
                  [6, 5, 3, 3, 2],
                  [3, 3, 5, 4, 9],
                  [3, 5, 3, 9, 0]]
        self.assertEqual(True, eight.tree_visible(forest, 2, 3))

    def test_when_tallest_col_tree_then_count_visible_trees(self):
        forest = [[0, 2, 3, 6, 5, 4]]
        self.assertEqual((3, 2), eight.count_tree_visible_col(forest, 0, 3))

    def test_when_shortest_col_tree_then_count_visible_trees(self):
        forest = [[0, 2, 3, 1, 5, 4]]
        self.assertEqual((1, 1), eight.count_tree_visible_col(forest, 0, 3))

    def test_when_tallest_row_tree_then_count_visible_trees(self):
        forest = [[0], [2], [3], [6], [5], [4]]
        self.assertEqual((3, 2), eight.count_tree_visible_row(forest, 3, 0))

    def test_when_shortest_row_tree_then_count_visible_trees(self):
        forest = [[0], [2], [3], [1], [5], [4]]
        self.assertEqual((1, 1), eight.count_tree_visible_row(forest, 3, 0))

