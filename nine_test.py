import unittest
import nine


class DayNineTests(unittest.TestCase):

    def test_me(self):
        self.assertTrue(True)

    def test_when_R_then_move_right(self):
        head = nine.Knot()
        head.move('R')
        self.assertEqual(1, head.x)
        self.assertEqual(0, head.y)

    def test_when_R_then_move_left(self):
        head = nine.Knot()
        head.move('L')
        self.assertEqual(-1, head.x)
        self.assertEqual(0, head.y)

    def test_when_R_then_move_up(self):
        head = nine.Knot()
        head.move('U')
        self.assertEqual(0, head.x)
        self.assertEqual(1, head.y)

    def test_when_R_then_move_down(self):
        head = nine.Knot()
        head.move('D')
        self.assertEqual(0, head.x)
        self.assertEqual(-1, head.y)

    def test_when_h_R1_t_then_t_still(self):
        head = nine.Knot()
        tail = nine.Knot()
        head.move('R')
        tail.follow(head)
        self.assertEqual(0, tail.x)
        self.assertEqual(0, tail.y)

    def test_when_h_R2_t_then_t_R1(self):
        head = nine.Knot()
        tail = nine.Knot()
        head.move('R')
        tail.follow(head)
        head.move('R')
        tail.follow(head)
        self.assertEqual(1, tail.x)
        self.assertEqual(0, tail.y)

    def test_when_h_L1_t_then_t_still(self):
        head = nine.Knot()
        tail = nine.Knot()
        head.move('L')
        tail.follow(head)
        self.assertEqual(0, tail.x)
        self.assertEqual(0, tail.y)

    def test_when_h_L2_t_then_t_L1(self):
        head = nine.Knot()
        tail = nine.Knot()
        head.move('L')
        tail.follow(head)
        head.move('L')
        tail.follow(head)
        self.assertEqual(-1, tail.x)
        self.assertEqual(0, tail.y)

    def test_when_h_U1_t_then_t_still(self):
        head = nine.Knot()
        tail = nine.Knot()
        head.move('U')
        tail.follow(head)
        self.assertEqual(0, tail.x)
        self.assertEqual(0, tail.y)

    def test_when_h_U2_t_then_t_U1(self):
        head = nine.Knot()
        tail = nine.Knot()
        head.move('U')
        tail.follow(head)
        head.move('U')
        tail.follow(head)
        self.assertEqual(0, tail.x)
        self.assertEqual(1, tail.y)

    def test_when_h_D1_t_then_t_still(self):
        head = nine.Knot()
        tail = nine.Knot()
        head.move('D')
        tail.follow(head)
        self.assertEqual(0, tail.x)
        self.assertEqual(0, tail.y)

    def test_when_h_D2_t_then_t_D1(self):
        head = nine.Knot()
        tail = nine.Knot()
        head.move('D')
        tail.follow(head)
        head.move('D')
        tail.follow(head)
        self.assertEqual(0, tail.x)
        self.assertEqual(-1, tail.y)

    def test_when_h_R1U1R1_t_then_t_R1U1(self):
        head = nine.Knot()
        tail = nine.Knot()
        head.move('R')
        tail.follow(head)
        head.move('U')
        tail.follow(head)
        head.move('R')
        tail.follow(head)
        self.assertEqual(1, tail.x)
        self.assertEqual(1, tail.y)

    def test_when_h_L1U1L1_t_then_t_L1U1(self):
        head = nine.Knot()
        tail = nine.Knot()
        head.move('L')
        tail.follow(head)
        head.move('U')
        tail.follow(head)
        head.move('L')
        tail.follow(head)
        self.assertEqual(-1, tail.x)
        self.assertEqual(1, tail.y)

    def test_when_h_R1D1R1_t_then_t_R1D1(self):
        head = nine.Knot()
        tail = nine.Knot()
        head.move('R')
        tail.follow(head)
        head.move('D')
        tail.follow(head)
        head.move('R')
        tail.follow(head)
        self.assertEqual(1, tail.x)
        self.assertEqual(-1, tail.y)

    def test_when_h_L1D1L1_t_then_t_L1D1(self):
        head = nine.Knot()
        tail = nine.Knot()
        head.move('L')
        tail.follow(head)
        head.move('D')
        tail.follow(head)
        head.move('L')
        tail.follow(head)
        self.assertEqual(-1, tail.x)
        self.assertEqual(-1, tail.y)

    def test_when_h_R4U4_t_then_t_R4U3(self):
        head = nine.Knot()
        tail = nine.Knot()
        head.move('R')
        tail.follow(head)
        head.move('R')
        tail.follow(head)
        head.move('R')
        tail.follow(head)
        head.move('R')
        tail.follow(head)
        head.move('U')
        tail.follow(head)
        head.move('U')
        tail.follow(head)
        head.move('U')
        tail.follow(head)
        head.move('U')
        tail.follow(head)

        tail.follow(head)
        self.assertEqual(4, tail.x)
        self.assertEqual(3, tail.y)

    def test_when_h_R2U2_t_then_t_R1U1(self):
        head = nine.Knot()
        tail = nine.Knot()
        head.move('R')
        head.move('R')
        head.move('U')
        head.move('U')
        tail.follow(head)
        self.assertEqual(1, tail.x)
        self.assertEqual(1, tail.y)



