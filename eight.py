def get_input(filename) -> list:
    forest = []
    with open(filename, 'r') as inp_file:
        inp_lines = inp_file.read().split('\n')

    for line in inp_lines:
        new_list = []
        new_list.extend(line)
        forest.append([int(item) for item in new_list])

    return forest


def count_edge_trees(forest: list) -> int:
    ret_val = len(forest) * 2 + len(forest[0]) * 2 - 4
    return ret_val


def tree_visible_col(forest: list, i: int, j: int) -> bool:
    visible_from_left = True
    visible_from_right = True
    # go left
    for col in reversed(range(0, j)):
        if forest[i][j] <= forest[i][col]:
            visible_from_left = False
            break
    # go right
    for col in range(j + 1, len(forest[i])):
        if forest[i][j] <= forest[i][col]:
            visible_from_right = False
            break

    return visible_from_left or visible_from_right


def tree_visible_row(forest: list, i: int, j: int) -> bool:
    visible_from_up = True
    visible_from_down = True
    # go up
    for row in reversed(range(0, i)):
        if forest[i][j] <= forest[row][j]:
            visible_from_up = False
    # go down
    for row in range(i + 1, len(forest)):
        if forest[i][j] <= forest[row][j]:
            visible_from_down = False
    return visible_from_up or visible_from_down


def tree_visible(forest: list, i: int, j: int):
    return tree_visible_row(forest, i, j) or tree_visible_col(forest, i, j)


def count_tree_visible_col(forest: list, i: int, j: int) -> (int, int):
    visible_trees_left = 0
    visible_trees_right = 0
    # go left
    for col in reversed(range(0, j)):
        if forest[i][j] <= forest[i][col]:
            visible_trees_left += 1
            break
        else:
            visible_trees_left += 1
    # go right
    for col in range(j + 1, len(forest[i])):
        if forest[i][j] <= forest[i][col]:
            visible_trees_right += 1
            break
        else:
            visible_trees_right += 1

    return visible_trees_left, visible_trees_right


def count_tree_visible_row(forest: list, i: int, j: int) -> (int, int):
    visible_trees_up = 0
    visible_trees_down = 0
    # go up
    for row in reversed(range(0, i)):
        if forest[i][j] <= forest[row][j]:
            visible_trees_up += 1
            break
        else:
            visible_trees_up += 1
    # go down
    for row in range(i + 1, len(forest)):
        if forest[i][j] <= forest[row][j]:
            visible_trees_down += 1
            break
        else:
            visible_trees_down += 1

    return visible_trees_up, visible_trees_down


def count_visible_trees(forest: list, i: int, j: int) -> int:
    u, d = count_tree_visible_row(forest, i, j)
    l, r = count_tree_visible_col(forest, i, j)
    return u * d * l * r


def count_inner_forest_trees(forest: list) -> int:
    visible_trees = 0
    for i in range(1, len(forest) - 1):
        for j in range(1, len(forest[i]) - 1):
            visible_trees += 1 if tree_visible(forest, i, j) else 0
    return visible_trees


def part_one():
    forest = get_input('input.txt')
    print(count_edge_trees(forest) + count_inner_forest_trees(forest))


def part_two():
    forest = get_input('input.txt')
    max_visibility_score = 0
    for i in range(1, len(forest) - 1):
        for j in range(1, len(forest[i]) - 1):
            max_visibility_score = max(max_visibility_score, count_visible_trees(forest, i, j))
    print(max_visibility_score)


import time
start_time = time.time()
part_one()
end_time = time.time()

print(end_time - start_time)
