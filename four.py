with open('input.txt', 'r') as inp_file:
    inp_str = inp_file.read()

inp_lines = inp_str.split('\n')


def part_one():
    count = 0
    for line in inp_lines:
        r1_str = line.split(',')[0]
        r2_str = line.split(',')[1]
        r1_num1 = int(r1_str.split('-')[0])
        r1_num2 = int(r1_str.split('-')[1])
        r2_num1 = int(r2_str.split('-')[0])
        r2_num2 = int(r2_str.split('-')[1])
        if r1_num1 <= r2_num1 and r1_num2 >= r2_num2 or r2_num1 <= r1_num1 and r2_num2 >= r1_num2:
            count += 1
    print(count)


def part_two():
    count = 0
    for line in inp_lines:
        r1_str = line.split(',')[0]
        r2_str = line.split(',')[1]
        r1_num1 = int(r1_str.split('-')[0])
        r1_num2 = int(r1_str.split('-')[1])
        r2_num1 = int(r2_str.split('-')[0])
        r2_num2 = int(r2_str.split('-')[1])
        if r1_num1 <= r2_num1 <= r1_num2 or r1_num1 <= r2_num2 <= r1_num2 or r2_num1 <= r1_num1 <= r2_num2 or r1_num1 <= r2_num2 <= r1_num2:
            count += 1

    print(count)

part_two()
